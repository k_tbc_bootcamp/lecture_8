package com.lkakulia.lecture_8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //sign up button listener
        signUpBtn.setOnClickListener({
            toastMessage("You cannot sign up yet")
        })

        //forgot password button listener
        forgotBtn.setOnClickListener({
            toastMessage("I don't know your password either")
        })

        //log in button listener
        logInBtn.setOnClickListener({
            if (email.text.isEmpty() && password.text.isEmpty())
                toastMessage("Email and password are empty")

            else if (email.text.isEmpty())
                toastMessage("Email is empty")

            else if (password.text.isEmpty())
                toastMessage("Password is empty")

            else if (isEmailValid(email.text.toString()))
                toastMessage("Login successful!")

            else
                toastMessage("Login not successful")
        })

    }

    //toast message display function
    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    //validate email function
    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}
